# ---- Base Node ---- #
FROM ubuntu:16.04 AS Base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool libleveldb-dev autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/LIMXTEC/Megacoin.git /opt/megacoin
RUN cd /opt/megacoin && \
	./autogen.sh && \
    ./configure --without-miniupnpc --disable-tests --without-gui && \
    make install


# ---- Release ---- #
FROM ubuntu:16.04 as release 
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y  libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r megacoin && useradd -r -m -g megacoin megacoin
RUN mkdir /data
RUN chown megacoin:megacoin /data
COPY --from=build /opt/megacoin/src/megacoind /usr/local/bin/
COPY --from=build /opt/megacoin/src/megacoin-cli /usr/local/bin/
USER megacoin
VOLUME /data
EXPOSE 58585 58583
CMD ["/usr/local/bin/megacoind",  "-datadir=/data", "-conf=/data/megacoin.conf", "-server", "-txindex", "-printtoconsole"]